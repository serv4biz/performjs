import PerformJS from "./core.js";
import Utility from "./utility.js";

PerformJS.prototype.html = function (...elemArgs) {
    if (elemArgs === undefined) {
        if (this.length > 0) {
            return this[0].innerHTML;
        }
        return "";
    }

    let htmlBuff = "";
    for (let nodeItem of Utility.parseHTMLNode(...elemArgs)) {
        htmlBuff += nodeItem;
    }

    // Eval run scripts //
    for (let elemItem of this) {
        elemItem.innerHTML = htmlBuff;
        let scripts = elemItem.querySelectorAll("script");
        for (let scriptItem of scripts) {
            eval(scriptItem.innerHTML);
        }
    }
    return this;
};

PerformJS.prototype.append = function (...elemArgs) {
    for (let elemItem of this) {
        for (let argItem of elemArgs) {
            if (typeof argItem === "string" || typeof argItem === "number" || typeof argItem === "boolean") {
                for (let nodeItem of Utility.parseHTMLNode(argItem)) {
                    let elem = document.createElement("div");
                    elem.innerHTML = nodeItem;
                    for (let childItem of elem.children) {
                        elemItem.append(childItem);

                        // Eval run scripts //
                        let scripts = childItem.querySelectorAll("script");
                        for (let scriptItem of scripts) {
                            eval(scriptItem.innerHTML);
                        }
                    }
                }
            } else if (Utility.isElement(argItem)) {
                elemItem.append(argItem);

                // Eval run scripts //
                let scripts = argItem.querySelectorAll("script");
                for (let scriptItem of scripts) {
                    eval(scriptItem.innerHTML);
                }
            } else if (Utility.isIterable(argItem)) {
                //this.append(...argItem);
            }
        }
    }
    return this;
};

PerformJS.prototype.width = function (width) {
    if (width === undefined) {
        if (this.length > 0) {
            return this[0].offsetWidth;
        }
        return 0;
    }

    for (let elemItem of this) {
        elemItem.offsetWidth = width;
    }
    return this;
};

PerformJS.prototype.height = function (height) {
    if (height === undefined) {
        if (this.length > 0) {
            return this[0].offsetHeight;
        }
        return 0;
    }

    for (let elemItem of this) {
        elemItem.offsetHeight = height;
    }
    return this;
};

export default PerformJS;