import PerformJS from "./core.js";

PerformJS.prototype.scrollTop = function (y) {
    if (y === undefined) {
        if (this.length > 0) {
            return this[0].scrollTop;
        }
        return 0;
    }

    for (let elemItem of this) {
        elemItem.scrollTop = y;
    }
    return this;
};

PerformJS.prototype.hide = function () {
    for (let elemItem of this) {
        elemItem.style.display = "none";
    }
    return this;
};

PerformJS.prototype.show = function () {
    for (let elemItem of this) {
        elemItem.style.display = null;
    }
    return this;
};

PerformJS.prototype.val = function (value) {
    if (value === undefined) {
        if (this.length > 0) {
            return this[0].value;
        }
        return "";
    }

    for (let elemItem of this) {
        elemItem.value = value;
    }
    return this;
};

// Attributes
PerformJS.prototype.attr = function () {
    if (this.length == 0) {
        return null;
    }

    if (arguments.length == 0) {
        return null;
    }

    if (arguments.length == 1) {
        let val = this[0].getAttribute(arguments[0]);
        if (val == null || val === undefined) {
            return null;
        }
        return val;
    }

    if (arguments.length == 2) {
        for (let elemItem of this) {
            if(arguments[1] == null) {
                elemItem.removeAttribute(arguments[0]);
            } else {
                elemItem.setAttribute(arguments[0], arguments[1]);
            }
        }
    }
    return this;
};

PerformJS.prototype.hasAttr = function (name) {
    if (name == null || name === undefined) {
        return false;
    }

    if (this.length == 0) {
        return false;
    }

    return this[0].hasAttribute(name);
};

PerformJS.prototype.removeAttr = function (name) {
    if (name == null || name === undefined) {
        return false;
    }

    if (this.length == 0) {
        return false;
    }

    this[0].removeAttribute(name);
    return this;
};

// Properties
PerformJS.prototype.prop = function () {
    if (this.length == 0) {
        return null;
    }

    if (arguments.length == 0) {
        return null;
    }

    if (arguments.length == 1) {
        return this[0][arguments[0]];
    }

    if (arguments.length == 2) {
        for (let elemItem of this) {
            elemItem[arguments[0]] = arguments[1];
        }
    }
    return this;
};

PerformJS.prototype.hasProp = function (name) {
    if (name == null || name === undefined) {
        return false;
    }

    if (this.length == 0) {
        return false;
    }

    return this[0].hasOwnProperty(name);
};

PerformJS.prototype.removeProp = function (name) {
    if (name == null || name === undefined) {
        return false;
    }

    if (this.length == 0) {
        return false;
    }

    delete this[0][name];
    return this;
};

export default PerformJS;