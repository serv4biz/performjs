import PerformJS from "./core.js"

PerformJS.prototype.addClass = function(...tokens) {
    for(let elemItem of this) {
        elemItem.classList.add(...tokens);
    }
    return this;
};

PerformJS.prototype.removeClass = function(...tokens) {
    for(let elemItem of this) {
        elemItem.classList.remove(...tokens);
    }
    return this;
};

PerformJS.prototype.replaceClass = function(oldToken,newToken) {
    for(let elemItem of this) {
        elemItem.classList.replace(oldToken, newToken);
    }
    return this;
};

PerformJS.prototype.toggleClass = function(token, force) {
    for(let elemItem of this) {
        elemItem.classList.toggle(token, force);
    }
    return this;
};

export default PerformJS