import PerformJS from "./core.js";

PerformJS.prototype.findId = function(elementId){
    let elemItem = null;
    if(this.length > 0) {
        for(let objItem of this) {
            elemItem = objItem.querySelector(`#${elementId}`);
            if(elemItem != null) {
                break;
            }
        }
    } else {
        elemItem = document.getElementById(elementId);
    }

    if(elemItem != null) {
        this.push(elemItem);
    }
    return this;
};

PerformJS.prototype.find = function(query){
    let elemItem = null;
    if(this.length > 0) {
        for(let objItem of this) {
            elemItem = objItem.querySelector(query);
            if(elemItem != null) {
                break;
            }
        }
    } else {
        elemItem = document.querySelector(query);
    }

    if(elemItem != null) {
        this.push(elemItem);
    }
    return this;
};

PerformJS.prototype.all = function(query){
    if(this.length > 0) {
        for(let objItem of this) {
            elemItem = objItem.querySelectorAll(query);
            this.push(elemItem);
        }
    } else {
        for(let elemItem of document.querySelectorAll(query)) {
            this.push(elemItem);
        }
    }
    return this;
};

PerformJS.prototype.allName = function(elementName){
    if(this.length > 0) {
        for(let objItem of this) {
            elemItem = objItem.querySelector(`[name="${elementName}"]`);
            this.push(elemItem);
        }
    } else {
        for(let elemItem of document.getElementsByName(elementName)) {
            this.push(elemItem);
        }
    }
    return this;
};

PerformJS.prototype.allClass = function(classNames){
    if(this.length > 0) {
        for(let objItem of this) {
            for(let elemItem of objItem.getElementsByClassName(classNames)) {
                this.push(elemItem);
            }
        }
    } else {
        for(let elemItem of document.getElementsByClassName(classNames)) {
            this.push(elemItem);
        }
    }
    return this;
};

PerformJS.prototype.allTag = function(tagName){
    if(this.length > 0) {
        for(let objItem of this) {
            for(let elemItem of objItem.getElementsByTagName(tagName)) {
                this.push(elemItem);
            }
        }
    } else {
        for(let elemItem of document.getElementsByTagName(tagName)) {
            this.push(elemItem);
        }
    }
    return this;
};

export default PerformJS