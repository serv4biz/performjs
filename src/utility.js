
function encodeHTMLEntities(val) {
    var textArea = document.createElement('textarea');
    textArea.innerText = val;
    return textArea.innerHTML;
}

function decodeHTMLEntities(val) {
    let textArea = document.createElement('textarea');
    textArea.innerHTML = val;
    return textArea.value;
}

function isIterable(obj) {
    if (obj == null || obj == undefined || !(obj instanceof Object)) {
        return false;
    }
    return Symbol.iterator in obj;
}

function isElement(element) {
    return element instanceof Element;
}

function isNode(element) {
    return element instanceof Node;
}

function parseHTMLNode(...htmlArgs) {
    let elem = document.createElement("div");
    for(let argItem of htmlArgs) {
        if(typeof argItem === "string" || typeof argItem === "number" || typeof argItem === "boolean"){
            elem.innerHTML += Utility.decodeHTMLEntities(String(argItem));
        } else if (isElement(argItem)) {
            elem.innerHTML += argItem.outerHTML;
        } else if (isNode(argItem)) {
            elem.innerHTML += argItem.nodeValue;
        } else if (isIterable(argItem)) {
            for (let nodeItem of parseHTMLNode(...argItem)) {
                elem.innerHTML += nodeItem;
            }
        }
    }
    
    let jsaNodes = [];
    for(let nodeItem of elem.childNodes) {
        switch(nodeItem.nodeType) {
            case Node.TEXT_NODE:
                jsaNodes.push(nodeItem.nodeValue);
                break;
            case Node.ELEMENT_NODE:
                jsaNodes.push(nodeItem.outerHTML);
                break;
        }
    }
    return jsaNodes;
}

function fromElements(elem,...elemArgs){
    for (let argItem of elemArgs) {
        if (typeof argItem === "string" || typeof argItem === "number" || typeof argItem === "boolean") {
            for(let nodeItem of Utility.parseHTMLNode(argItem)) {
                let elem = document.createElement("div");
                elem.innerHTML = nodeItem;
                for(let elemItem of elem.children) {
                    this.push(elemItem);
                }
            }
        } else if (Utility.isElement(argItem)) {
            this.push(argItem);
        } else if (Utility.isIterable(argItem)) {
            this.fromElements(...argItem);
        }
    }
};

export default {
    encodeHTMLEntities,
    decodeHTMLEntities,
    parseHTMLNode,
    isIterable,
    isElement,
    isNode,
    fromElements
}