import Http from "./http.js";

function fetch(url) {
    return new Promise(function (resolve, reject) {
        fetch(url).then(function (res) {
            let headers = {};
            for (let keyName of res.headers.keys()) {
                headers[keyName] = res.headers.get(keyName).toLowerCase();
            }

            let results = {};
            res.text().then(function (data) {
                results.status = res.status;
                results.statusText = Http.statusText(res.status);
                if (results.statusText == "") {
                    results.statusText = res.statusText;
                }
                results.data = data;
                results.headers = headers;

                if (res.ok) {
                    results.ok = true;
                    resolve(results);
                } else {
                    results.ok = false;
                    reject(results);
                }
            });
        }, function (e) {
            let results = {};
            results.status = 0;
            results.statusText = e.message;
            results.data = "";
            results.headers = {};
            results.ok = false;
            reject(results);
        });
    });
};

function get(url) {
    return Ajax.fetch(url);
};

function request(url, params = {}) {
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

    let urlencoded = new URLSearchParams();
    for (let keyName in params) {
        urlencoded.append(keyName, params[keyName]);
    }

    let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded
    };

    return new Promise(function (resolve, reject) {
        fetch(url, requestOptions).then(function (res) {
            let headers = {};
            for (let keyName of res.headers.keys()) {
                headers[keyName] = res.headers.get(keyName).toLowerCase();
            }

            let results = {};
            res.text().then(function (data) {
                results.status = res.status;
                results.statusText = Http.statusText(res.status);
                if (results.statusText == "") {
                    results.statusText = res.statusText;
                }
                results.data = data;
                results.headers = headers;

                if (res.ok) {
                    results.ok = true;
                    resolve(results);
                } else {
                    results.ok = false;
                    reject(results);
                }
            });
        }, function (e) {
            let results = {};
            results.status = 0;
            results.statusText = e.message;
            results.data = "";
            results.headers = {};
            results.ok = false;
            reject(results);
        });
    });
};

function post(url, params = {}) {
    return Ajax.request(url, params);
};

function upload(url, formdata = new FormData()) {
    let requestOptions = {
        method: 'POST',
        body: formdata
    };

    return new Promise(function (resolve, reject) {
        fetch(url, requestOptions).then(function (res) {
            let headers = {};
            for (let keyName of res.headers.keys()) {
                headers[keyName] = res.headers.get(keyName).toLowerCase();
            }

            let results = {};
            res.text().then(function (data) {
                results.status = res.status;
                results.statusText = Http.statusText(res.status);
                if (results.statusText == "") {
                    results.statusText = res.statusText;
                }
                results.data = data;
                results.headers = headers;

                if (res.ok) {
                    results.ok = true;
                    resolve(results);
                } else {
                    results.ok = false;
                    reject(results);
                }
            });
        }, function (e) {
            let results = {};
            results.status = 0;
            results.statusText = e.message;
            results.data = "";
            results.headers = {};
            results.ok = false;
            reject(results);
        });
    });
};

export default {
    fetch,
    get,
    request,
    post,
    upload
}