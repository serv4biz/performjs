import PerformJS from "./core.js";

PerformJS.prototype.on = function(type,listener, options){
    for(let elemItem of this) {
        elemItem.addEventListener(type,listener,options);
    }
    return this;
};

PerformJS.prototype.off = function(type,listener, options) {
    for(let elemItem of this) {
        elemItem.removeEventListener(type,listener,options);
    }
    return this;
};

PerformJS.prototype.click = function(callback){
    for(let elemItem of this) {
        if(callback === undefined) {
            elemItem.click();
        } else {
            elemItem.addEventListener("click",(event)=>{
                callback(event);
            });
        }
    }
    return this;
};

PerformJS.prototype.dblclick = function(callback){
    for(let elemItem of this) {
        if(callback === undefined) {
            elemItem.dblclick();
        } else {
            elemItem.addEventListener("dblclick",(event)=>{
                callback(event);
            });
        }
    }
    return this;
};

PerformJS.prototype.change = function(callback){
    for(let elemItem of this) {
        if(callback === undefined) {
            elemItem.change();
        } else {
            elemItem.addEventListener("change",(event)=>{
                callback(event);
            });
        }
    }
    return this;
};

export default PerformJS