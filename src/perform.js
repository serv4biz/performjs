
import PerformJS from "./core.js";
import PerformJSAjax from "./ajax.js";

import "./selector.js";
import "./event.js";
import "./property.js";
import "./style.js";
import "./dom.js";

window.$ajax = PerformJSAjax;

window.$px = function(...elems){
    return new PerformJS(elems);
};

window.$id = function(elementId){
    return new PerformJS().findId(elementId);
};

window.$name = function(elementName){
    return new PerformJS().allName(elementName);
};

window.$class = function(classNames){
    return new PerformJS().allClass(classNames);
};

window.$tag = function(tagName){
    return new PerformJS().allTag(tagName);
};

window.$find = function(query){
    return new PerformJS().find(query);
};

window.$all = function(query){
    return new PerformJS().all(query);
};