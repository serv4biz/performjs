import Utility from "./utility.js";

class PerformJS extends Array {
    constructor(...elemArgs) {
        super();
        this.fromElements(elemArgs);
    }
}

PerformJS.prototype.fromElements = function(...elemArgs){
    for (let argItem of elemArgs) {
        if (typeof argItem === "string" || typeof argItem === "number" || typeof argItem === "boolean") {
            for(let nodeItem of Utility.parseHTMLNode(argItem)) {
                let elem = document.createElement("div");
                elem.innerHTML = nodeItem;
                for(let elemItem of elem.children) {
                    this.push(elemItem);
                }
            }
        } else if (Utility.isElement(argItem)) {
            this.push(argItem);
        } else if (Utility.isIterable(argItem)) {
            this.fromElements(...argItem);
        }
    }
};

PerformJS.prototype.clear = function () {
    while (this.length > 0) {
        this.shift();
    }
}

PerformJS.prototype.delete = function (index = 0) {
    if (this.length > 0) {
        this.splice(index, 1);
    }
}

export default PerformJS